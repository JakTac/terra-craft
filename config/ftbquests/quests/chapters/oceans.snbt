{
	id: "70C94F6CCF38734E"
	group: ""
	order_index: 2
	filename: "oceans"
	title: "Shining Seas"
	icon: "minecraft:heart_of_the_sea"
	subtitle: ["About 80% of our oceans remain unexplored. Lets fix that"]
	default_quest_shape: ""
	default_hide_dependency_lines: false
	quests: [
		{
			title: "The Seven +3 Seas"
			icon: "minecraft:sea_lantern"
			x: -20.0d
			y: -9.5d
			subtitle: "Go to all 10 ocean biomes "
			dependencies: ["4C463A7ACD13FFF2"]
			hide: true
			id: "72E105347ECED5EE"
			tasks: [
				{
					id: "1B1F5B1063F3E36F"
					type: "biome"
					title: "Ocean"
					biome: "minecraft:ocean"
				}
				{
					id: "0A03F41830F16AFB"
					type: "biome"
					title: "Cold Ocean"
					biome: "minecraft:cold_ocean"
				}
				{
					id: "071A00FB1A58141B"
					type: "biome"
					title: "Deep Cold Ocean"
					biome: "minecraft:deep_cold_ocean"
				}
				{
					id: "3329CA82ECACDF4D"
					type: "biome"
					title: "Deep Frozen Ocean"
					biome: "minecraft:deep_frozen_ocean"
				}
				{
					id: "017C7BBAE3F6C8D2"
					type: "biome"
					title: "Deep Lukewarm Ocean"
					biome: "minecraft:deep_lukewarm_ocean"
				}
				{
					id: "6572BD54435C2E1E"
					type: "biome"
					title: "Deep Ocean"
					biome: "minecraft:deep_ocean"
				}
				{
					id: "7522D23E7548F63C"
					type: "biome"
					title: "Deep Warm Ocean"
					biome: "minecraft:deep_warm_ocean"
				}
				{
					id: "4B27EBB7FE799ACF"
					type: "biome"
					title: "Frozen Ocean"
					biome: "minecraft:frozen_ocean"
				}
				{
					id: "34011779C1DB283D"
					type: "biome"
					title: "Lukewarm Ocean"
					biome: "minecraft:lukewarm_ocean"
				}
				{
					id: "44BAEBDDA8A6385A"
					type: "biome"
					title: "Warm Ocean"
					biome: "minecraft:warm_ocean"
				}
			]
			rewards: [{
				id: "390CB3FEAECACC2A"
				type: "xp_levels"
				xp_levels: 10
			}]
		}
		{
			title: "A Watery Grave"
			icon: "minecraft:dark_oak_boat"
			x: -15.5d
			y: -9.5d
			subtitle: "Find a sunken ship and pillage it's treasures"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			dependency_requirement: "one_started"
			id: "2CD00A8266F371F7"
			tasks: [{
				id: "2F622C3FB2AB3990"
				type: "structure"
				title: "Shipwreck"
				structure: "minecraft:shipwreck"
			}]
			rewards: [{
				id: "00AACD75B639CE2D"
				type: "xp_levels"
				xp_levels: 2
			}]
		}
		{
			title: "Pirate Booty"
			x: -13.5d
			y: -9.5d
			subtitle: "Find a buried treasure map"
			dependencies: ["2CD00A8266F371F7"]
			hide: true
			id: "3C76E8CC1E85F973"
			tasks: [{
				id: "1334CED2C3EAFFA2"
				type: "item"
				title: "Buried Treasure Map"
				item: "minecraft:filled_map"
			}]
			rewards: [{
				id: "440DBB299D5CC8BB"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			x: -11.5d
			y: -9.5d
			subtitle: "Find buried treasure using a map"
			dependencies: ["3C76E8CC1E85F973"]
			hide: true
			id: "38490A13F72928CB"
			tasks: [{
				id: "4A13AC413EB536E2"
				type: "structure"
				title: "X Marks the Spot"
				icon: "minecraft:barrier"
				structure: "minecraft:mineshaft"
			}]
			rewards: [{
				id: "2CE415999D9C7830"
				type: "xp_levels"
				xp_levels: 4
			}]
		}
		{
			title: "A Beating Heart"
			x: -9.5d
			y: -9.5d
			subtitle: "Obtain a heart of the sea"
			dependencies: ["38490A13F72928CB"]
			hide: true
			id: "4926C2E695FCF666"
			tasks: [{
				id: "61005292CB9E715B"
				type: "item"
				item: "minecraft:heart_of_the_sea"
			}]
			rewards: [{
				id: "2AD8EAD1957058E7"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Atlantis?"
			icon: "upgrade_aquatic:chiseled_prismarine_coralstone"
			x: -20.0d
			y: -2.0d
			subtitle: "Find an ocean monument"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			dependency_requirement: "one_started"
			id: "74EF9892681007B7"
			tasks: [{
				id: "6A6E569A39400613"
				type: "structure"
				title: "Ocean Temple"
				icon: "upgrade_aquatic:chiseled_prismarine_coralstone"
				structure: "minecraft:monument"
			}]
			rewards: [{
				id: "6183E69E60AF67C5"
				type: "item"
				item: {
					id: "minecraft:potion"
					Count: 1b
					tag: {
						Potion: "minecraft:long_water_breathing"
					}
				}
				count: 3
			}]
		}
		{
			title: "Seashells by the Seashore"
			x: -11.5d
			y: -7.0d
			subtitle: "Collect a nautilus shell"
			dependencies: [
				"622DF4489236DC24"
				"0D1C755872CC1C99"
				"324C7C15AA95D42B"
			]
			hide: true
			dependency_requirement: "one_completed"
			id: "5A72E6A8AECC6557"
			tasks: [{
				id: "62E1ECA3743F5108"
				type: "item"
				item: "minecraft:nautilus_shell"
			}]
			rewards: [{
				id: "7FDBD79EA81DC7A2"
				type: "item"
				item: "minecraft:nautilus_shell"
				count: 2
			}]
		}
		{
			title: "Elder Evil"
			x: -11.5d
			y: -2.0d
			subtitle: "Wipe out the three elder guardians from a sea monument"
			dependencies: ["0F70ABD92BAB8573"]
			hide: true
			id: "1421C413546534D0"
			tasks: [{
				id: "583004CE6A192669"
				type: "kill"
				entity: "minecraft:elder_guardian"
				value: 3L
			}]
			rewards: [{
				id: "7DC3327636299507"
				type: "xp_levels"
				xp_levels: 10
			}]
		}
		{
			title: "In Need of a Orthopedist"
			x: -9.5d
			y: -1.0d
			subtitle: "Obtain an elder guardian spine. Basically a guardian spine but stronger"
			dependencies: [
				"0E3B08B54EC865BC"
				"1421C413546534D0"
			]
			hide: true
			id: "2199289A48F15C42"
			tasks: [{
				id: "3648309995CBADF1"
				type: "item"
				item: "upgrade_aquatic:elder_guardian_spine"
			}]
			rewards: [{
				id: "16CAD87C2D1EE849"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Now Cataracts Free!"
			x: -9.5d
			y: -2.0d
			subtitle: "Obtain an elder eye from a elder guardian"
			dependencies: ["1421C413546534D0"]
			hide: true
			id: "0C662E27FD406F20"
			tasks: [{
				id: "2D15565F5D45893E"
				type: "item"
				item: "upgrade_aquatic:elder_eye"
			}]
			rewards: [
				{
					id: "3A160A2F146437DA"
					type: "xp_levels"
					xp_levels: 5
				}
				{
					id: "5469F2679A2D1D54"
					type: "item"
					item: "artifacts:aqua_dashers"
				}
			]
		}
		{
			title: "Lord Helix Forgive Me"
			x: -15.5d
			y: -7.0d
			subtitle: "Slay a nautilus to get its shell"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			id: "0D1C755872CC1C99"
			tasks: [{
				id: "785E01B553DC4F72"
				type: "kill"
				entity: "upgrade_aquatic:nautilus"
				value: 1L
			}]
			rewards: [{
				id: "3799172A550B147E"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "Oxygen Wasn't Included"
			x: -15.5d
			y: -6.0d
			subtitle: "Slay a drowned"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			id: "324C7C15AA95D42B"
			tasks: [{
				id: "28356552133B3284"
				type: "kill"
				entity: "minecraft:drowned"
				value: 1L
			}]
			rewards: [{
				id: "0F860B23FC85548D"
				type: "xp_levels"
				xp_levels: 2
			}]
		}
		{
			title: "Fossil Finder"
			x: -15.5d
			y: -8.0d
			subtitle: "Find and mine embedded ammonite in underwater caves"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			id: "622DF4489236DC24"
			tasks: [{
				id: "5FFE28274709F5C1"
				type: "observation"
				title: "Embedded Ammonite"
				icon: "upgrade_aquatic:embedded_ammonite"
				timer: 0L
				observe_type: 0
				to_observe: "upgrade_aquatic:embedded_ammonite"
			}]
			rewards: [{
				id: "61B14001E7E03F9F"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "Poseidon's Power"
			x: -7.5d
			y: -9.5d
			subtitle: "Combine 8 nautilus shells and a heart of the sea to make a conduit"
			dependencies: [
				"4926C2E695FCF666"
				"5A72E6A8AECC6557"
			]
			hide: true
			id: "0E86EBD4456F31E8"
			tasks: [{
				id: "7F9E2599CD74C211"
				type: "item"
				item: "minecraft:conduit"
			}]
			rewards: [
				{
					id: "46FA3BD45625EC64"
					type: "item"
					item: "minecraft:dark_prismarine"
					count: 42
				}
				{
					id: "016D06CAFC5AC9E2"
					type: "item"
					item: "artifacts:flippers"
				}
			]
		}
		{
			title: "Shell Stone"
			x: -7.5d
			y: -7.0d
			subtitle: "Obtain coralstone, which can absorb the color of coral placed next to it without destroying or drying out the coral."
			dependencies: ["5A72E6A8AECC6557"]
			hide: true
			id: "37E2CA484D7F3D8F"
			tasks: [{
				id: "0544AAB8145D3058"
				type: "item"
				item: "upgrade_aquatic:coralstone"
			}]
			rewards: [{
				id: "4605BC7864EE262F"
				type: "item"
				item: "upgrade_aquatic:coralstone"
				count: 64
			}]
		}
		{
			title: "Thoughtless Thrashing"
			x: -15.5d
			y: -5.0d
			subtitle: "Slay a thrasher"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			id: "67278DF806791841"
			tasks: [{
				id: "04663B6079D0D2C6"
				type: "kill"
				entity: "upgrade_aquatic:thrasher"
				value: 1L
			}]
			rewards: [{
				id: "08D4342A4CEB5580"
				type: "item"
				item: "upgrade_aquatic:thrasher_tooth"
			}]
		}
		{
			title: "Guardian Down!"
			x: -15.5d
			y: -2.0d
			subtitle: "Slay a guardian"
			dependencies: ["74EF9892681007B7"]
			hide: true
			id: "0F70ABD92BAB8573"
			tasks: [{
				id: "72BF339E50B5570B"
				type: "kill"
				entity: "minecraft:guardian"
				value: 1L
			}]
			rewards: [{
				id: "4431694A143264D9"
				type: "item"
				item: "minecraft:prismarine_shard"
				count: 3
			}]
		}
		{
			title: "Three Pronged Fork"
			x: -11.5d
			y: -5.0d
			subtitle: "Obtain a trident via drowned or crafting one "
			dependencies: [
				"7B0970DC75EDC00E"
				"551FDBB693645787"
				"324C7C15AA95D42B"
			]
			hide: true
			dependency_requirement: "one_completed"
			id: "2DA5C0D4C788A480"
			tasks: [{
				id: "451235FBFC7ED372"
				type: "item"
				item: {
					id: "minecraft:trident"
					Count: 1b
					tag: {
						Damage: 0
					}
				}
			}]
			rewards: [{
				id: "775755BCFBE422A6"
				type: "choice"
				table_id: 4051137079295460959L
			}]
		}
		{
			title: "Practical Prismarine"
			x: -13.5d
			y: -3.0d
			subtitle: "Make a prismarine shard out of prismarine shards"
			dependencies: ["0F70ABD92BAB8573"]
			hide: true
			id: "551FDBB693645787"
			tasks: [{
				id: "59B3FDD1E23835B6"
				type: "item"
				item: "upgrade_aquatic:prismarine_rod"
			}]
			rewards: [{
				id: "2E8EB57E0BF5E962"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "A Dentist's Nightmare"
			x: -13.5d
			y: -4.0d
			subtitle: "Collect three thrasher teeth"
			dependencies: ["67278DF806791841"]
			hide: true
			id: "7B0970DC75EDC00E"
			tasks: [{
				id: "62C3DC4DF2323639"
				type: "item"
				item: "upgrade_aquatic:thrasher_tooth"
				count: 3L
			}]
			rewards: [{
				id: "75D2561A9AAAB660"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Grow a Spine"
			x: -13.5d
			y: -1.0d
			subtitle: "Obtain a guardian spine. Used to make spike traps"
			dependencies: ["0F70ABD92BAB8573"]
			hide: true
			id: "0E3B08B54EC865BC"
			tasks: [{
				id: "66046C4F1F2C5CC4"
				type: "item"
				item: "upgrade_aquatic:guardian_spine"
			}]
			rewards: [{
				id: "5F4EC26A9CC037DB"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "Found Him!"
			x: -15.5d
			y: -11.0d
			subtitle: "Collect a tropical fish"
			dependencies: ["72E105347ECED5EE"]
			hide: true
			id: "516FF804B0892B8F"
			tasks: [{
				id: "421DD07CB817E3EF"
				type: "item"
				item: "minecraft:tropical_fish"
			}]
			rewards: [{
				id: "4F9CD94EE2783561"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "You Are a Pirate"
			x: -20.0d
			y: -12.0d
			subtitle: "Complete every quest in this chapter"
			hide_dependency_lines: true
			dependencies: [
				"516FF804B0892B8F"
				"0E86EBD4456F31E8"
				"37E2CA484D7F3D8F"
				"2DA5C0D4C788A480"
				"0C662E27FD406F20"
				"2199289A48F15C42"
			]
			id: "42A9BD546A78C941"
			tasks: [{
				id: "085B23F2714B0EBC"
				type: "checkmark"
				title: "Click Me"
			}]
			rewards: [{
				id: "4C1E483D692C018C"
				type: "item"
				item: "artifacts:snorkel"
			}]
		}
	]
}
