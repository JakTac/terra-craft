import mods.jei.JEI;
craftingTable.removeRecipe(<item:doomangelring:itemdoomangelring>);
craftingTable.removeRecipe(<item:ars_nouveau:ritual_sunrise>);
craftingTable.removeRecipe(<item:theabyss:unstable_obsidian>);
craftingTable.removeRecipe(<item:theabyss:the_abyss>);
craftingTable.removeRecipe(<item:xkdeco:steel_armor_helmet>);
craftingTable.removeRecipe(<item:xkdeco:steel_armor_chestplate>);
craftingTable.removeRecipe(<item:xkdeco:steel_armor_leggings>);
craftingTable.removeRecipe(<item:xkdeco:steel_armor_boots>);
craftingTable.removeRecipe(<item:xkdeco:steel_block>);
craftingTable.removeRecipe(<item:xkdeco:steel_ingot>);
craftingTable.removeRecipe(<item:xkdeco:steel_tiles>);
craftingTable.removeRecipe(<item:xkdeco:hollow_steel_frame>);
craftingTable.removeRecipe(<item:xkdeco:steel_windmill>);
craftingTable.removeRecipe(<item:xkdeco:ventiduct>);
craftingTable.removeRecipe(<item:xkdeco:fan_blade>);
furnace.removeRecipe(<item:xkdeco:steel_ingot>);
blastFurnace.removeRecipe(<item:xkdeco:steel_ingot>);
mods.iceandfire.DragonForge.removeFireRecipe(<item:iceandfire:dragonsteel_fire_ingot>);
mods.iceandfire.DragonForge.removeIceRecipe(<item:iceandfire:dragonsteel_ice_ingot>);
mods.iceandfire.DragonForge.removeLightningRecipe(<item:iceandfire:dragonsteel_lightning_ingot>);
mods.iceandfire.DragonForge.addFireRecipe(<item:iceandfire:dragonsteel_fire_ingot>, <item:alloyed:steel_ingot>, <item:iceandfire:fire_dragon_blood>);
mods.iceandfire.DragonForge.addIceRecipe(<item:iceandfire:dragonsteel_ice_ingot>, <item:alloyed:steel_ingot>, <item:iceandfire:ice_dragon_blood>);
mods.iceandfire.DragonForge.addLightningRecipe(<item:iceandfire:dragonsteel_lightning_ingot>, <item:alloyed:steel_ingot>, <item:iceandfire:lightning_dragon_blood>);
craftingTable.addShaped("unstable_obsidian", <item:theabyss:unstable_obsidian> * 2, [
    [<item:minecraft:obsidian>, <item:theabyss:loran>, <item:minecraft:obsidian>],
    [<item:theabyss:loran>, <item:create:shadow_steel>, <item:theabyss:loran>],
    [<item:minecraft:obsidian>, <item:theabyss:loran>, <item:minecraft:obsidian>]
]);
craftingTable.addShaped("portal_activator", <item:theabyss:the_abyss>, [
    [<item:theabyss:loran>, <item:twilightforest:lamp_of_cinders>, <item:theabyss:loran>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:ars_nouveau:wilden_tribute>, <item:minecraft:air>]
]);
craftingTable.addShaped("luminous_prismarine", <item:upgrade_aquatic:luminous_prismarine>, [
    [<item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>],
    [<item:minecraft:prismarine_shard>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:prismarine_shard>],
    [<item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>]
]);
craftingTable.addShaped("elder_prismarine", <item:quark:elder_prismarine>, [
    [<item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>],
    [<item:minecraft:prismarine_shard>, <item:minecraft:white_dye>, <item:minecraft:prismarine_shard>],
    [<item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>, <item:minecraft:prismarine_shard>]
]);
craftingTable.addShaped("luminous_elder_prismarine", <item:upgrade_aquatic:luminous_elder_prismarine> * 4, [
    [<item:quark:elder_prismarine>, <item:quark:elder_prismarine>, <item:quark:elder_prismarine>],
    [<item:quark:elder_prismarine>, <item:cavesandcliffs:glow_ink_sac>, <item:quark:elder_prismarine>],
    [<item:quark:elder_prismarine>, <item:quark:elder_prismarine>, <item:quark:elder_prismarine>],
]);
craftingTable.addShaped("steel_helmet", <item:xkdeco:steel_armor_helmet>, [
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("steel_helmet2", <item:xkdeco:steel_armor_helmet>, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("steel_chestplate", <item:xkdeco:steel_armor_chestplate>, [
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("steel_leggings", <item:xkdeco:steel_armor_leggings>, [
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("steel_boots", <item:xkdeco:steel_armor_boots>, [
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("steel_boots2", <item:xkdeco:steel_armor_boots>, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("steel_block", <item:xkdeco:steel_block>, [
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShapeless("steel_block2steel", <item:alloyed:steel_ingot> * 9, [<item:xkdeco:steel_block>]);
craftingTable.addShaped("steel_tiles", <item:xkdeco:steel_tiles>, [
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("hollow_steel_frame", <item:xkdeco:hollow_steel_frame>, [
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:minecraft:air>, <item:alloyed:steel_ingot>, <item:minecraft:air>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("steel_windmill", <item:xkdeco:steel_windmill>, [
    [<item:minecraft:air>, <item:xkdeco:steel_trapdoor>, <item:minecraft:air>],
    [<item:xkdeco:steel_trapdoor>, <item:alloyed:steel_ingot>, <item:xkdeco:steel_trapdoor>],
    [<item:minecraft:air>, <item:xkdeco:steel_trapdoor>, <item:minecraft:air>]
]);
craftingTable.addShaped("ventiduct", <item:xkdeco:ventiduct>, [
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>],
    [<item:alloyed:steel_ingot>, <item:minecraft:air>, <item:alloyed:steel_ingot>]
]);
craftingTable.addShaped("fan_blade", <item:xkdeco:steel_windmill>, [
    [<item:minecraft:air>, <item:minecraft:heavy_weighted_pressure_plate>, <item:minecraft:air>],
    [<item:minecraft:heavy_weighted_pressure_plate>, <item:alloyed:steel_ingot>, <item:minecraft:heavy_weighted_pressure_plate>],
    [<item:minecraft:air>, <item:minecraft:heavy_weighted_pressure_plate>, <item:minecraft:air>]
]);
craftingTable.addShapeless("scoria2scoria", <item:byg:scoria_stone> * 1, [<item:create:natural_scoria>]);
craftingTable.addShapeless("scoria3scoria", <item:create:natural_scoria> * 1, [<item:byg:scoria_stone>]);
craftingTable.addShapeless("limestone2limestone", <item:create:weathered_limestone> * 1, [<item:quark:limestone>]);
craftingTable.addShapeless("limestone3limestone", <item:quark:limestone> * 1, [<item:create:weathered_limestone>]);
craftingTable.addShapeless("lavastone2soapstone", <item:extcaves:lavastone> * 1, [<item:byg:soapstone>]);
craftingTable.addShapeless("soapstone2lavastone", <item:byg:soapstone> * 1, [<item:extcaves:lavastone>]);
craftingTable.addShapedMirrored("white_jelly_torch", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:white_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:white_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("white_jelly_torch2", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:white_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:white_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("white_jelly_torch3", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:white_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:white_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("white_jelly_torch4", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:white_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:white_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("white_jelly_torch5", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:white_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("white_jelly_torch6", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:white_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("white_jelly_torch7", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:minecraft:white_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("white_jelly_torch8", <item:upgrade_aquatic:white_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:white_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("red_jelly_torch", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:red_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:red_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("red_jelly_torch2", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:red_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:red_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("red_jelly_torch3", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:red_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:red_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("red_jelly_torch4", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:red_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:red_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("red_jelly_torch5", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:red_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("red_jelly_torch6", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:red_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("red_jelly_torch7", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:minecraft:red_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("red_jelly_torch8", <item:upgrade_aquatic:red_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:red_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("orange_jelly_torch", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:orange_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:orange_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("orange_jelly_torch2", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:orange_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:orange_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("orange_jelly_torch3", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:orange_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:orange_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("orange_jelly_torch4", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:orange_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:orange_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("orange_jelly_torch5", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:orange_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("orange_jelly_torch6", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:orange_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("orange_jelly_torch7", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:minecraft:orange_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("orange_jelly_torch8", <item:upgrade_aquatic:orange_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:orange_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("yellow_jelly_torch", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:yellow_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("yellow_jelly_torch2", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:yellow_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("yellow_jelly_torch3", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:yellow_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("yellow_jelly_torch4", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:yellow_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("yellow_jelly_torch5", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:yellow_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("yellow_jelly_torch6", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("yellow_jelly_torch7", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:minecraft:yellow_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("yellow_jelly_torch8", <item:upgrade_aquatic:yellow_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:yellow_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("green_jelly_torch", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:green_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:green_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("green_jelly_torch2", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:green_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:green_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("green_jelly_torch3", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:green_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:green_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("green_jelly_torch4", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:green_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:green_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("green_jelly_torch5", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:green_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("green_jelly_torch6", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:green_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("green_jelly_torch7", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:minecraft:green_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("green_jelly_torch8", <item:upgrade_aquatic:green_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:green_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("blue_jelly_torch", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:blue_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:blue_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("blue_jelly_torch2", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:blue_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:blue_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("blue_jelly_torch3", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:blue_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:blue_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("blue_jelly_torch4", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:blue_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:blue_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("blue_jelly_torch5", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:blue_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("blue_jelly_torch6", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:blue_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("blue_jelly_torch7", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:minecraft:blue_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("blue_jelly_torch8", <item:upgrade_aquatic:blue_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:blue_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("purple_jelly_torch", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:purple_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:purple_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("purple_jelly_torch2", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:purple_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:purple_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("purple_jelly_torch3", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:purple_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:purple_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("purple_jelly_torch4", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:purple_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:purple_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("purple_jelly_torch5", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:purple_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("purple_jelly_torch6", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:purple_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("purple_jelly_torch7", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:minecraft:purple_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("purple_jelly_torch8", <item:upgrade_aquatic:purple_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:purple_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("pink_jelly_torch", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:pink_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:pink_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("pink_jelly_torch2", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>],
    [<item:minecraft:pink_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:pink_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("pink_jelly_torch3", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:pink_dye>, <item:cavesandcliffs:glow_ink_sac>, <item:minecraft:pink_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShapedMirrored("pink_jelly_torch4", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:pink_dye>, <item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:pink_dye>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:air>, <item:minecraft:air>]
]);
craftingTable.addShaped("pink_jelly_torch5", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:pink_dye>, <item:upgrade_aquatic:glowing_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("pink_jelly_torch6", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:upgrade_aquatic:glowing_ink_sac>, <item:minecraft:pink_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
craftingTable.addShaped("pink_jelly_torch7", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:minecraft:pink_dye>, <item:cavesandcliffs:glow_ink_sac>],
    [<item:minecraft:air>, <item:upgrade_aquatic:prismarine_rod>]
]);
craftingTable.addShaped("pink_jelly_torch8", <item:upgrade_aquatic:pink_jelly_torch> * 4, [
    [<item:cavesandcliffs:glow_ink_sac>, <item:minecraft:pink_dye>],
    [<item:upgrade_aquatic:prismarine_rod>, <item:minecraft:air>]
]);
<tag:items:twilightforest:portal/activator>.add(<item:betterendforge:eternal_crystal>);
<tag:items:twilightforest:portal/activator>.remove(<item:minecraft:diamond>);