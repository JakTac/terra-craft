Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    25518      9157       0          3424       0.36
structure_refs      18506      1744       0          97         0.09
biomes              18505      26594      0          486        1.44
terrain             18502      34743      1          90         1.88
surface             18497      15420      0          97         0.83
air_carvers         18492      35941      0          196        1.94
fluid_carvers       18485      4688       0          85         0.25
decoration          17479      211893     5          1080       12.12
mob_spawns          16428      436        0          15         0.03
structure_searches  0          0          0          0          0.00
Sum                                                             18.95