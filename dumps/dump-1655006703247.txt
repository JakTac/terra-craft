Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    6245       8508       0          4137       1.36
structure_refs      2845       341        0          23         0.12
biomes              2845       1200       0          521        0.42
terrain             2842       7594       1          124        2.67
surface             2842       3926       0          155        1.38
air_carvers         2842       6954       0          88         2.45
fluid_carvers       2840       499        0          20         0.18
decoration          2507       42355      6          1145       16.89
mob_spawns          2212       170        0          18         0.08
structure_searches  0          0          0          0          0.00
Sum                                                             25.55