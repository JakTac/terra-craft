Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    52         11         0          6          0.21
structure_refs      114        24         0          0          0.21
biomes              114        4345       0          3712       38.11
terrain             114        344        1          21         3.02
surface             114        187        0          19         1.64
air_carvers         114        597        1          89         5.24
fluid_carvers       114        8          0          0          0.07
decoration          114        2493       9          265        21.87
mob_spawns          104        8          0          1          0.08
structure_searches  0          0          0          0          0.00
Sum                                                             70.45