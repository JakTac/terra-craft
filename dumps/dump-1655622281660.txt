Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    27698      9252       0          3388       0.33
structure_refs      19378      1734       0          11         0.09
biomes              19378      47395      0          1554       2.45
terrain             19377      35798      1          1110       1.85
surface             19376      16143      0          77         0.83
air_carvers         19376      39181      0          1152       2.02
fluid_carvers       19376      2901       0          70         0.15
decoration          18372      247620     5          1345       13.48
mob_spawns          17365      792        0          84         0.05
structure_searches  0          0          0          0          0.00
Sum                                                             21.25