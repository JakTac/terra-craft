Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    7686       1812       0          603        0.24
structure_refs      4652       479        0          2          0.10
biomes              4652       9617       0          4271       2.07
terrain             4651       10100      1          106        2.17
surface             4651       5499       0          109        1.18
air_carvers         4651       10243      0          110        2.20
fluid_carvers       4652       702        0          244        0.15
decoration          4373       62703      5          370        14.34
mob_spawns          4104       321        0          113        0.08
structure_searches  0          0          0          0          0.00
Sum                                                             22.53