Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    37948      2864       0          1072       0.08
structure_refs      30076      2622       0          4          0.09
biomes              30076      41711      0          3755       1.39
terrain             30076      53985      1          66         1.79
surface             30073      23795      0          227        0.79
air_carvers         30072      60563      0          1264       2.01
fluid_carvers       30071      4348       0          43         0.14
decoration          29113      352133     5          1222       12.10
mob_spawns          28123      1088       0          78         0.04
structure_searches  0          0          0          0          0.00
Sum                                                             18.43