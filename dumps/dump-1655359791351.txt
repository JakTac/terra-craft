Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    14674      3575       0          2747       0.24
structure_refs      10932      906        0          2          0.08
biomes              10932      36697      0          1533       3.36
terrain             10931      20013      1          85         1.83
surface             10931      8890       0          70         0.81
air_carvers         10931      25500      0          89         2.33
fluid_carvers       10930      118        0          6          0.01
decoration          10464      155387     7          1533       14.85
mob_spawns          9969       582        0          21         0.06
structure_searches  3          0          0          0          0.00
Sum                                                             23.58