Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    117        2654       0          2638       22.68
structure_refs      7          7          0          3          1.00
biomes              7          869        0          867        124.14
terrain             7          62         4          23         8.86
surface             7          44         3          13         6.29
air_carvers         7          133        3          48         19.00
fluid_carvers       7          2          0          1          0.29
decoration          0          0          0          0          0.00
mob_spawns          0          0          0          0          0.00
structure_searches  0          0          0          0          0.00
Sum                                                             182.26