Section             Count      Time MS    Min MS     Max MS     Average MS
structure_starts    435        3405       0          3184       7.83
structure_refs      50         39         0          18         0.78
biomes              50         705        0          405        14.10
terrain             50         164        1          22         3.28
surface             50         142        0          16         2.84
air_carvers         50         341        1          76         6.82
fluid_carvers       50         7          0          1          0.14
decoration          18         830        11         183        46.11
mob_spawns          2          0          0          0          0.00
structure_searches  0          0          0          0          0.00
Sum                                                             81.90