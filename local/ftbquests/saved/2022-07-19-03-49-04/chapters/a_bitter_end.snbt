{
	id: "0294576A8509B15C"
	group: ""
	order_index: 12
	filename: "a_bitter_end"
	title: "A Bitter End"
	icon: "minecraft:ender_eye"
	subtitle: ["After delving deep in The Nether I've discovered strange places, with plants and fauna that seem almost alien. These things must come from somewhere right?"]
	default_quest_shape: ""
	default_hide_dependency_lines: false
	quests: [
		{
			icon: "minecraft:mossy_stone_bricks"
			x: 0.0d
			y: 0.0d
			subtitle: "Locate a stronghold"
			dependencies: ["71AF3F197136EED2"]
			hide: true
			id: "7B3D7604AF8117EF"
			tasks: [{
				id: "6ECA782832A604F2"
				type: "advancement"
				title: "Eye Spy"
				advancement: "minecraft:story/follow_ender_eye"
				criterion: ""
			}]
			rewards: [{
				id: "1ADCC1BCB3FFBED2"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			icon: "minecraft:end_portal_frame"
			x: 2.5d
			y: 0.0d
			subtitle: "Enter the End portal"
			dependencies: ["7B3D7604AF8117EF"]
			id: "2F24BB1AD44C41F6"
			tasks: [{
				id: "18BBDADD53B38EB2"
				type: "advancement"
				title: "The End?"
				advancement: "minecraft:story/enter_the_end"
				criterion: ""
			}]
			rewards: [{
				id: "476520BF31C8BAB6"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			x: 4.5d
			y: -1.0d
			subtitle: "Slay the Ender Dragon"
			dependencies: ["2F24BB1AD44C41F6"]
			hide: true
			id: "79E11FDA6DB87E80"
			tasks: [{
				id: "43896F8D615A71FA"
				type: "advancement"
				title: "The End"
				advancement: "minecraft:end/kill_dragon"
				criterion: ""
			}]
			rewards: [{
				id: "3DFAA2CB22F35A72"
				type: "item"
				item: {
					id: "minecraft:enchanted_book"
					Count: 1b
					tag: {
						StoredEnchantments: [{
							lvl: 1s
							id: "betterendforge:end_veil"
						}]
					}
				}
			}]
		}
		{
			title: "You Need a Mint"
			x: 4.5d
			y: 1.5d
			subtitle: "Collect dragon's breath in a glass bottle"
			dependencies: ["2F24BB1AD44C41F6"]
			id: "01B0ACFCC0343E5D"
			tasks: [{
				id: "16A7AD84226A7CB5"
				type: "item"
				item: "minecraft:dragon_breath"
			}]
			rewards: [{
				id: "5CB0B92D0EB934F1"
				type: "xp_levels"
				xp_levels: 7
			}]
		}
		{
			x: 7.0d
			y: -1.0d
			subtitle: "Escape the island"
			dependencies: ["79E11FDA6DB87E80"]
			hide: true
			id: "1CE5041C64887BB7"
			tasks: [{
				id: "2DF3DBD114EE12EA"
				type: "advancement"
				title: "Remote Getaway"
				advancement: "minecraft:end/enter_end_gateway"
				criterion: ""
			}]
			rewards: [{
				id: "54E5375AA6148B3B"
				type: "item"
				item: "betterendforge:guidebook"
			}]
		}
		{
			x: 7.0d
			y: 1.0d
			subtitle: "Hold the dragon egg"
			dependencies: ["79E11FDA6DB87E80"]
			hide: true
			id: "7F28BAF93172D9E4"
			tasks: [{
				id: "05A63DA1BDBCC3D3"
				type: "advancement"
				title: "The Next Generation"
				advancement: "minecraft:end/dragon_egg"
				criterion: ""
			}]
			rewards: [{
				id: "4E29346FB7EA394A"
				type: "xp_levels"
				xp_levels: 15
			}]
		}
		{
			x: 7.0d
			y: -3.0d
			subtitle: "Summon the ender dragon "
			dependencies: ["79E11FDA6DB87E80"]
			hide: true
			id: "62BC219FFF726CBF"
			tasks: [{
				id: "34BB220B07221B7E"
				type: "advancement"
				title: "The End... Again..."
				advancement: "minecraft:end/respawn_dragon"
				criterion: ""
			}]
			rewards: [{
				id: "196A0F4ED4FF5459"
				type: "xp_levels"
				xp_levels: 10
			}]
		}
		{
			x: 10.5d
			y: -2.0d
			subtitle: "Discover a end city"
			dependencies: ["1CE5041C64887BB7"]
			hide: true
			id: "0981569E7DEDA564"
			tasks: [{
				id: "3FA09CBBE5986F63"
				type: "advancement"
				title: "The Eternal City"
				advancement: "minecraft:end/find_end_city"
				criterion: ""
			}]
			rewards: [{
				id: "6795C5DC0C437C34"
				type: "xp_levels"
				xp_levels: 7
			}]
		}
		{
			x: 9.0d
			y: -3.5d
			subtitle: "Levitate up 50 blocks from shulker attacks"
			dependencies: ["0981569E7DEDA564"]
			hide: true
			id: "0AE576EA4D79A742"
			tasks: [{
				id: "2D1DB56C05969039"
				type: "advancement"
				title: "Great View From Up Here"
				advancement: "minecraft:end/levitate"
				criterion: ""
			}]
			rewards: [{
				id: "356C4C855F74BE43"
				type: "xp_levels"
				xp_levels: 15
			}]
		}
		{
			x: 12.0d
			y: -3.5d
			subtitle: "Obtain a elytra"
			hide_dependency_lines: false
			dependencies: ["0981569E7DEDA564"]
			hide: true
			id: "4DD7D32D2F596FEE"
			tasks: [{
				id: "151EA305361FA0FA"
				type: "advancement"
				title: "Sky's the Limit"
				advancement: "minecraft:end/elytra"
				criterion: ""
			}]
			rewards: [{
				id: "27FF572355ED483F"
				type: "item"
				item: {
					id: "minecraft:enchanted_book"
					Count: 1b
					tag: {
						StoredEnchantments: [{
							lvl: 4s
							id: "minecraft:unbreaking"
						}]
					}
				}
			}]
		}
		{
			title: "Flavourite"
			x: 10.5d
			y: 0.0d
			subtitle: "Collect flavolite, a useful stone, in the end"
			dependencies: ["1CE5041C64887BB7"]
			hide: true
			id: "727675B9D117ED83"
			tasks: [{
				id: "170917F3EE6AF829"
				type: "item"
				item: "betterendforge:flavolite"
			}]
			rewards: [{
				id: "15DEE373ED64EF9D"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "Piece by Piece"
			x: 13.0d
			y: 0.0d
			subtitle: "Collect ender shards from ender ore"
			dependencies: ["727675B9D117ED83"]
			hide: true
			dependency_requirement: "one_completed"
			id: "7A277628B78CAB8A"
			tasks: [{
				id: "3FABDC62AEDFDDD5"
				type: "item"
				item: "betterendforge:ender_shard"
			}]
			rewards: [{
				id: "04F0E3624E90B650"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "A Bit More Than a Mallet"
			x: 15.5d
			y: 1.5d
			subtitle: "Make a hammer"
			dependencies: ["7A277628B78CAB8A"]
			hide: true
			id: "26EECCE7F1C3A3E7"
			tasks: [{
				id: "2F40C8A949DD90F2"
				type: "item"
				title: "Any Hammer From Better End"
				item: {
					id: "itemfilters:tag"
					Count: 1b
					tag: {
						value: "betterendforge:hammers"
					}
				}
			}]
			rewards: [{
				id: "6F9E80AC7574524D"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Alien Alloy"
			x: 15.5d
			y: -1.5d
			subtitle: "Get a thallasium ingot by smelting thallasium ore"
			dependencies: ["7A277628B78CAB8A"]
			hide: true
			id: "23FA0179AC0F0B1C"
			tasks: [{
				id: "239629C6E1678E1B"
				type: "item"
				item: "betterendforge:thallasium_ingot"
			}]
			rewards: [{
				id: "70CDB0D700641B22"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Reduced to Atoms"
			x: 18.0d
			y: 1.5d
			subtitle: "Take a hammer to a end crystal or ender pearl in a anvil to make ender dust"
			dependencies: ["26EECCE7F1C3A3E7"]
			hide: true
			id: "41865C204465FA89"
			tasks: [{
				id: "42343DC8C48F8B58"
				type: "item"
				item: "betterendforge:ender_dust"
			}]
			rewards: [{
				id: "1A88AE4DD7C65E48"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Determinite"
			x: 20.5d
			y: 0.0d
			subtitle: "Create terminite by smelting thallasium with ender dust"
			dependencies: [
				"41865C204465FA89"
				"124D716EC41BB053"
			]
			hide: true
			id: "3B544F353F5C39BA"
			tasks: [{
				id: "3DE675E7A0F5303A"
				type: "item"
				item: "betterendforge:terminite_ingot"
			}]
			rewards: [{
				id: "4CDB1E84A1D31B78"
				type: "xp_levels"
				xp_levels: 8
			}]
		}
		{
			title: "A Different Kind of \"Super Smelter\""
			x: 18.0d
			y: -1.5d
			subtitle: "Make a end stone smelter with thallasium"
			dependencies: ["23FA0179AC0F0B1C"]
			hide: true
			id: "124D716EC41BB053"
			tasks: [{
				id: "324F8302E2E448B2"
				type: "item"
				item: "betterendforge:end_stone_smelter"
			}]
			rewards: [{
				id: "6A5882A33C3B2B8F"
				type: "xp_levels"
				xp_levels: 7
			}]
		}
		{
			title: "Ancient Alien Alloy"
			x: 23.5d
			y: -2.0d
			subtitle: "Make an aeternium ingot by smelting terminite with netherite in a end stone smelter"
			dependencies: ["3B544F353F5C39BA"]
			id: "07F8FF57752E9686"
			tasks: [{
				id: "53AF6520B85E7E37"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
			rewards: [{
				id: "1E7AC8556E6260DE"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Infusion Dance"
			x: 10.5d
			y: 2.0d
			subtitle: "Make a infusion pedestal"
			dependencies: ["1CE5041C64887BB7"]
			hide: true
			id: "01C6B7D593C0010C"
			tasks: [{
				id: "66D3BB62AA8997A9"
				type: "item"
				item: "betterendforge:infusion_pedestal"
			}]
			rewards: [{
				id: "299DDE60053E5E7B"
				type: "xp_levels"
				xp_levels: 3
			}]
		}
		{
			title: "The Ritual is Almost Complete"
			x: 12.0d
			y: 3.5d
			subtitle: "Make 8 flavolite pedestals (although any pedestal from betterend can be used for infusions)"
			dependencies: [
				"01C6B7D593C0010C"
				"727675B9D117ED83"
			]
			hide: true
			id: "3E356EF993DA3EA2"
			tasks: [{
				id: "6C83DD4C30785792"
				type: "item"
				item: "betterendforge:flavolite_pedestal"
				count: 8L
			}]
			rewards: [{
				id: "102E160A06C6F4D0"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Taste the Rainbow"
			x: 18.0d
			y: 3.5d
			subtitle: "Collect crystal shards from the crystal mountains biome"
			dependencies: ["3E356EF993DA3EA2"]
			hide: true
			id: "78B3BA3E8D05BA7A"
			tasks: [{
				id: "124D1026579E59ED"
				type: "item"
				item: "betterendforge:crystal_shards"
			}]
			rewards: [{
				id: "4BB7DE4F7FAB7E2D"
				type: "xp_levels"
				xp_levels: 5
			}]
		}
		{
			title: "Ancient Alien Runes"
			x: 23.5d
			y: 3.5d
			subtitle: "Infuse crystal shards into polished flavolite to make runed flavolite so that you can repair eternal portals"
			dependencies: ["78B3BA3E8D05BA7A"]
			hide: true
			id: "3C7E3F65DE7546BA"
			tasks: [{
				id: "00DD8481715DAFA9"
				type: "item"
				item: "betterendforge:flavolite_runed"
			}]
			rewards: [{
				id: "53D529947203065B"
				type: "xp_levels"
				xp_levels: 7
			}]
		}
		{
			title: "Eternally Awesome"
			x: 23.5d
			y: 5.5d
			subtitle: "Create eternal shards via infusion to light eternal portals between The End and The Overworld and possibly to somewhere else..."
			dependencies: ["78B3BA3E8D05BA7A"]
			hide: true
			id: "7372AB72477EBD4A"
			tasks: [{
				id: "4B52505CB9518B3E"
				type: "item"
				item: "betterendforge:eternal_crystal"
			}]
			rewards: [{
				id: "15CBC4B95EF30A2B"
				type: "item"
				item: {
					id: "patchouli:guide_book"
					Count: 1b
					tag: {
						"patchouli:book": "twilightforest:guide"
					}
				}
			}]
		}
		{
			title: "Painvil"
			x: 26.0d
			y: -0.5d
			subtitle: "Make a aeternium anvil to make some tools and weapons"
			dependencies: ["07F8FF57752E9686"]
			hide: true
			id: "27D64E8F9E3F7D4F"
			tasks: [{
				id: "4BF7017179D0AF50"
				type: "item"
				item: {
					id: "betterendforge:aeternium_anvil"
					Count: 1b
					tag: { }
				}
			}]
			rewards: [{
				id: "5135AA1D287CD021"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Cover me in Crystals"
			x: 23.5d
			y: 1.5d
			subtitle: "Get a full suit of crystalite armor"
			dependencies: [
				"78B3BA3E8D05BA7A"
				"7AEA2628096DED4C"
			]
			hide: true
			id: "1A86A1F2E24177FA"
			tasks: [
				{
					id: "4ACCD931F9F497BE"
					type: "item"
					item: {
						id: "betterendforge:crystalite_helmet"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "0E01D35D3F7C77A7"
					type: "item"
					item: {
						id: "betterendforge:crystalite_chestplate"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "6933F171FC5260E1"
					type: "item"
					item: {
						id: "betterendforge:crystalite_leggings"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "1F05359F1E8DD21B"
					type: "item"
					item: {
						id: "betterendforge:crystalite_boots"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
			]
			rewards: [{
				id: "64C73DEEC16C810F"
				type: "xp_levels"
				xp_levels: 15
			}]
		}
		{
			title: "Cover me in Terminite"
			x: 23.5d
			y: 0.0d
			dependencies: ["3B544F353F5C39BA"]
			id: "7AEA2628096DED4C"
			tasks: [
				{
					id: "671A193CB8FC4000"
					type: "item"
					item: {
						id: "betterendforge:terminite_helmet"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "3F9E06E4CAAFFA1F"
					type: "item"
					item: {
						id: "betterendforge:terminite_chestplate"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "12219861D90BF718"
					type: "item"
					item: {
						id: "betterendforge:terminite_leggings"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "2538877FBBFC1A55"
					type: "item"
					item: {
						id: "betterendforge:terminite_boots"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
			]
			rewards: [{
				id: "4A5FAF4BE4343232"
				type: "xp_levels"
				xp_levels: 10
			}]
		}
		{
			title: "Cover me in Aeternium"
			x: 26.0d
			y: -3.5d
			subtitle: "Get a full suit of aeternium armor"
			dependencies: ["07F8FF57752E9686"]
			hide: true
			id: "147B607748D3E215"
			tasks: [
				{
					id: "4DE59417BEBED115"
					type: "item"
					item: {
						id: "betterendforge:aeternium_helmet"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "20D2DCE0A024DD28"
					type: "item"
					item: {
						id: "betterendforge:aeternium_chestplate"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "26C3EB5810179D70"
					type: "item"
					item: {
						id: "betterendforge:aeternium_leggings"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
				{
					id: "2B6072EA6E44A54E"
					type: "item"
					item: {
						id: "betterendforge:aeternium_boots"
						Count: 1b
						tag: {
							Damage: 0
						}
					}
				}
			]
			rewards: [{
				id: "5142D5D71FFB539E"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "The End Explorer"
			icon: "architects_palette:ender_pearl_block"
			x: 2.5d
			y: 7.5d
			subtitle: "Explore every biome in The End"
			dependencies: [
				"0F54D541EB0C81E3"
				"59486848A187B1C3"
				"6EA34575A2DE96AA"
				"31FD523BC764C90D"
				"311BBEBEDEE924B3"
				"3914DBB42D0EC0D5"
				"30E3123F0BBC5B00"
				"5FA38BA653EDA133"
			]
			dependency_requirement: "one_started"
			id: "60396E3C5E8E95E7"
			tasks: [
				{
					id: "7946BA8197318868"
					type: "biome"
					title: "The End"
					biome: "minecraft:the_end"
				}
				{
					id: "4549AE53197F01E4"
					type: "biome"
					title: "End Barrens"
					biome: "minecraft:end_barrens"
				}
				{
					id: "60769804A67DF207"
					type: "biome"
					title: "End Highlands"
					biome: "minecraft:end_highlands"
				}
				{
					id: "22E4B04C54924DE3"
					type: "biome"
					title: "End Midlands"
					biome: "minecraft:end_midlands"
				}
				{
					id: "52DCB351742D01FC"
					type: "biome"
					title: "Megalake"
					biome: "betterendforge:megalake"
				}
				{
					id: "35DA6422B9F95516"
					type: "biome"
					title: "Megalake Grove"
					biome: "betterendforge:megalake_grove"
				}
				{
					id: "5E0EFD6BD1CCE17F"
					type: "biome"
					title: "Sulphur Springs"
					biome: "betterendforge:sulphur_springs"
				}
				{
					id: "625D1657D5FC5FB0"
					type: "biome"
					title: "Ice Starfield"
					biome: "betterendforge:ice_starfield"
				}
				{
					id: "3EF325789FE2B906"
					type: "biome"
					title: "Painted Mountains"
					biome: "betterendforge:painted_mountains"
				}
				{
					id: "4751F1656A8F0329"
					type: "biome"
					title: "Crystal Mountains"
					biome: "betterendforge:crystal_mountains"
				}
				{
					id: "436AAC54215BCB67"
					type: "biome"
					title: "Blossoming Spires"
					biome: "betterendforge:blossoming_spires"
				}
				{
					id: "6A063A02E3849282"
					type: "biome"
					title: "Dust Wastelands"
					biome: "betterendforge:dust_wastelands"
				}
				{
					id: "15A70B627E9363AD"
					type: "biome"
					title: "Neon Oasis"
					biome: "betterendforge:neon_oasis"
				}
				{
					id: "47D1BC58B495001C"
					type: "biome"
					title: "Dry Shrubland"
					biome: "betterendforge:dry_shrubland"
				}
				{
					id: "4C4990775ABB32EB"
					type: "biome"
					title: "Dragon Graveyards"
					biome: "betterendforge:dragon_graveyards"
				}
				{
					id: "0E8A1AFBA1CD7A9F"
					type: "biome"
					title: "Foggy Mushroomland"
					biome: "betterendforge:foggy_mushroomland"
				}
				{
					id: "54A377B239AD0674"
					type: "biome"
					title: "Umbrella Jungle"
					biome: "betterendforge:umbrella_jungle"
				}
				{
					id: "3176FC0E1F356D56"
					type: "biome"
					title: "Amber Land"
					biome: "betterendforge:amber_land"
				}
				{
					id: "647B58ED5BF416F1"
					type: "biome"
					title: "Lantern Woods"
					biome: "betterendforge:lantern_woods"
				}
				{
					id: "41E10CCD25FB08FE"
					type: "biome"
					title: "Glowing Grasslands"
					biome: "betterendforge:glowing_grasslands"
				}
				{
					id: "468108658658976D"
					type: "biome"
					title: "Chorus Forests"
					biome: "betterendforge:chorus_forest"
				}
				{
					id: "470E85C335FDE503"
					type: "biome"
					title: "Shadow Forest"
					biome: "betterendforge:shadow_forest"
				}
				{
					id: "1B950262D5C21625"
					type: "biome"
					title: "Purpur Peaks"
					biome: "byg:purpur_peaks"
				}
				{
					id: "25FDD65A1748F4BD"
					type: "biome"
					title: "Shattered Desert"
					biome: "byg:shattered_desert"
				}
				{
					id: "1FE517B20B25EF37"
					type: "biome"
					title: "Ivis Fields"
					biome: "byg:ivis_fields"
				}
				{
					id: "254FCC7F3521A3B6"
					type: "biome"
					title: "Viscal Isles"
					biome: "byg:viscal_isles"
				}
				{
					id: "6C0AED1E5A44574A"
					type: "biome"
					title: "Cryptic Wastes"
					biome: "byg:cryptic_wastes"
				}
				{
					id: "6A0E4166AE74D4AB"
					type: "biome"
					title: "Imparius Grove"
					biome: "byg:imparius_grove"
				}
				{
					id: "42F25250DA4A383E"
					type: "biome"
					title: "Bulbis Gardens"
					biome: "byg:bulbis_gardens"
				}
				{
					id: "75A8B026098329F6"
					type: "biome"
					title: "Nightshade Forest"
					biome: "byg:nightshade_forest"
				}
				{
					id: "1E75FDF698410BE9"
					type: "biome"
					title: "Shulkren Forest"
					biome: "byg:shulkren_forest"
				}
				{
					id: "5991E5EF483E1742"
					type: "biome"
					title: "Ethereal Islands"
					biome: "byg:ethereal_islands"
				}
			]
			rewards: [{
				id: "1E56D4CF339A521D"
				type: "item"
				item: "betterendforge:aeternium_block"
			}]
		}
		{
			title: "A Barren Land"
			icon: "minecraft:end_stone"
			x: 2.5d
			y: 4.5d
			subtitle: "Visit the end midlands, end highlands, end barrens, and the end in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "0F54D541EB0C81E3"
			tasks: [
				{
					id: "00CCAB2C34589D20"
					type: "biome"
					title: "End Midlands"
					biome: "minecraft:end_midlands"
				}
				{
					id: "05EC1B1D564FD335"
					type: "biome"
					title: "End Barrens"
					biome: "minecraft:end_barrens"
				}
				{
					id: "6ED561AE30E36C64"
					type: "biome"
					title: "End Highlands"
					biome: "minecraft:end_highlands"
				}
				{
					id: "65FE72487C79EAFB"
					type: "biome"
					title: "The End"
					biome: "minecraft:the_end"
				}
			]
			rewards: [{
				id: "01FBC75C547785D1"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Perposterous Ponds"
			icon: "betterendforge:end_lily_leaf"
			x: 5.5d
			y: 7.5d
			subtitle: "Visit the megalake, megalake grove, sulphur springs, and ice starfields in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "30E3123F0BBC5B00"
			tasks: [
				{
					id: "541D6DB9833539B1"
					type: "biome"
					title: "Megalake"
					biome: "betterendforge:megalake"
				}
				{
					id: "02B45D7F38A38414"
					type: "biome"
					title: "Megalake Grove"
					biome: "betterendforge:megalake_grove"
				}
				{
					id: "0688FB94A67B63B2"
					type: "biome"
					title: "Sulphur Springs"
					biome: "betterendforge:sulphur_springs"
				}
				{
					id: "5D44CB3CBB1B81CA"
					type: "biome"
					title: "Ice Starfield"
					biome: "betterendforge:ice_starfield"
				}
			]
			rewards: [{
				id: "77105360CABD1F58"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Colorful Cliffs"
			icon: "betterendforge:aurora_crystal"
			x: -0.5d
			y: 7.5d
			subtitle: "Visit purpur peaks, painted mountains, crystal cliffs, and blossoming spires in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "6EA34575A2DE96AA"
			tasks: [
				{
					id: "27E278348563715C"
					type: "biome"
					title: "Purpur Peaks"
					biome: "byg:purpur_peaks"
				}
				{
					id: "4332D47593179319"
					type: "biome"
					title: "Painted Mountains"
					biome: "betterendforge:painted_mountains"
				}
				{
					id: "0A22B53D24429B23"
					type: "biome"
					title: "Crystal Mountains"
					biome: "betterendforge:crystal_mountains"
				}
				{
					id: "3108140C3313E647"
					type: "biome"
					title: "Blossoming Spires"
					biome: "betterendforge:blossoming_spires"
				}
			]
			rewards: [{
				id: "79950A1A2F34AB27"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Dusty Desert"
			icon: "betterendforge:endstone_dust"
			x: 2.5d
			y: 10.5d
			subtitle: "Visit the shattered desert, dust wastelands, neon oasis, and dry shrubland in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "31FD523BC764C90D"
			tasks: [
				{
					id: "2D5EF28A7416C474"
					type: "biome"
					title: "Shattered Desert"
					biome: "byg:shattered_desert"
				}
				{
					id: "43098FCAE76960D7"
					type: "biome"
					title: "Dust Wastelands"
					biome: "betterend:dust_wastelands"
				}
				{
					id: "20BA97AE51A0F7BD"
					type: "biome"
					title: "Neon Oasis"
					biome: "betterendforge:neon_oasis"
				}
				{
					id: "74FBEFD2C25EE956"
					type: "biome"
					title: "Dry Shrubland"
					biome: "betterendforge:dry_shrubland"
				}
			]
			rewards: [{
				id: "715439C684A59579"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Garnet Growths"
			icon: "betterendforge:mossy_obsidian"
			x: 0.5d
			y: 9.5d
			subtitle: "Visit the dragon graveyards, ivis fields, viscal isles, and cryptic wastes in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "311BBEBEDEE924B3"
			tasks: [
				{
					id: "7F6D4C0F5099D423"
					type: "biome"
					title: "Dragon Graveyards"
					biome: "betterendforge:dragon_graveyards"
				}
				{
					id: "6206CD54A33DD505"
					type: "biome"
					title: "Ivis Fields"
					biome: "byg:ivis_fields"
				}
				{
					id: "5E349DBE53AEE44D"
					type: "biome"
					title: "Viscal Isles"
					biome: "byg:viscal_isles"
				}
				{
					id: "0F990312AE2D340C"
					type: "biome"
					title: "Cryptic Wastes"
					biome: "byg:cryptic_wastes"
				}
			]
			rewards: [{
				id: "17F641312EF29DBE"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Towering Toadstools"
			icon: "betterendforge:mossy_glowshroom_hymenophore"
			x: 4.5d
			y: 9.5d
			subtitle: "Visit the imparius grove, foggy mushroomland, umbrella jungle, and bulbis gardens in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "3914DBB42D0EC0D5"
			tasks: [
				{
					id: "1E35286A021D94A3"
					type: "biome"
					title: "Imparius Grove"
					biome: "byg:imparius_grove"
				}
				{
					id: "6B78B9D7DAC62DFB"
					type: "biome"
					title: "Foggy Mushroomland"
					biome: "betterendforge:foggy_mushroomland"
				}
				{
					id: "144192ABF8507A73"
					type: "biome"
					title: "Umbrella Jungle"
					biome: "betterendforge:umbrella_jungle"
				}
				{
					id: "4FE72363036FE682"
					type: "biome"
					title: "Bulbis Gardens"
					biome: "byg:bulbis_gardens"
				}
			]
			rewards: [{
				id: "6A82CAED79AD117E"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Wild Woods"
			icon: "betterendforge:amber_moss"
			x: 0.5d
			y: 5.5d
			subtitle: "Visit the amber lands, lantern woods, glowing grasslands, and nightshade forest in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "5FA38BA653EDA133"
			tasks: [
				{
					id: "643C360E293F11D5"
					type: "biome"
					title: "Amber Land"
					biome: "betterendforge:amber_land"
				}
				{
					id: "438D139688517919"
					type: "biome"
					title: "Lantern Woods"
					biome: "betterendforge:lantern_woods"
				}
				{
					id: "217CE3AFEB829559"
					type: "biome"
					title: "Glowing Grasslands"
					biome: "betterendforge:glowing_grasslands"
				}
				{
					id: "1394AEEA228D9012"
					type: "biome"
					title: "Nightshade Forest"
					biome: "byg:nightshade_forest"
				}
			]
			rewards: [{
				id: "4B461FDDB420F81B"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "Lavender Lands"
			icon: "betterendforge:chorus_nylium"
			x: 4.5d
			y: 5.5d
			subtitle: "Visit the shulkren forest, chorus forest, shadow forest, and ethereal islands in The End"
			hide_dependency_lines: true
			dependencies: ["1CE5041C64887BB7"]
			id: "59486848A187B1C3"
			tasks: [
				{
					id: "44A87668576E44F4"
					type: "biome"
					title: "Shulkren Forest"
					biome: "byg:shulkren_forest"
				}
				{
					id: "5452EE566DF8267C"
					type: "biome"
					title: "Chorus Forest"
					biome: "betterendforge:chorus_forest"
				}
				{
					id: "65CB406C1E5514DB"
					type: "biome"
					title: "Shadow Forest"
					biome: "byg:shadow_forest"
				}
				{
					id: "39AAE50FA6F517FF"
					type: "biome"
					title: "Ethereal Islands"
					biome: "byg:ethereal_islands"
				}
			]
			rewards: [{
				id: "39F6A38C6A322CA2"
				type: "item"
				item: "betterendforge:aeternium_ingot"
			}]
		}
		{
			title: "A Gateway Between Worlds"
			icon: "twilightforest:force_field_purple"
			x: 28.0d
			y: 4.5d
			subtitle: "Find an eternal portal to travel between The End and The Overworld"
			dependencies: [
				"7372AB72477EBD4A"
				"3C7E3F65DE7546BA"
			]
			id: "18DB0AE4AE449543"
			tasks: [{
				id: "445456D6BD49444B"
				type: "structure"
				title: "Eternal Portal"
				structure: "betterendforge:eternal_portal_structure"
			}]
			rewards: [{
				id: "3C7B3ED684497D81"
				type: "xp_levels"
				xp_levels: 10
			}]
		}
		{
			title: "The End of The End"
			x: 0.0d
			y: -2.0d
			subtitle: "Complete every quest in this chapter"
			hide_dependency_lines: true
			dependencies: [
				"01B0ACFCC0343E5D"
				"60396E3C5E8E95E7"
				"7F28BAF93172D9E4"
				"62BC219FFF726CBF"
				"0AE576EA4D79A742"
				"4DD7D32D2F596FEE"
				"1A86A1F2E24177FA"
				"18DB0AE4AE449543"
				"27D64E8F9E3F7D4F"
				"147B607748D3E215"
			]
			hide: true
			id: "4906C64A0CD8FF9E"
			tasks: [{
				id: "7AEC6B187E28BED5"
				type: "checkmark"
				title: "Click Me"
			}]
			rewards: [{
				id: "3EEE0E3B5CAB3605"
				type: "item"
				item: "betterendforge:aeternium_block"
			}]
		}
	]
}
